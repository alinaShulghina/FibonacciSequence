package com.softserve.edu.fibonacci;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by alin- on 16.12.2017.
 */
public class FibonacciSequenceTest {

    private FibonacciSequence fibonacciSequence;
    private List<Integer> numbers;

    @Test
    public void getNumbersTestFrom1To100() {
        numbers = Arrays.asList(2, 3, 5, 8, 13, 21, 34, 55, 89);
        fibonacciSequence = new FibonacciSequence(1, 100);
        assertEquals(numbers, fibonacciSequence.getNumbers());
    }

    @Test
    public void getNumbersTestFrom100To2000() {
        numbers = Arrays.asList(144, 233, 377, 610, 987, 1597);
        fibonacciSequence = new FibonacciSequence(100, 2000);
        assertEquals(numbers, fibonacciSequence.getNumbers());
    }

    @Test
    public void getNumbersTestNegativeNumbers() {
        numbers = new ArrayList<>();
        fibonacciSequence = new FibonacciSequence(-100, 0);
        assertEquals(numbers, fibonacciSequence.getNumbers());
    }

    @Test
    public void getNumbersTestFrom1To5000() {
        numbers = Arrays.asList(2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181);
        fibonacciSequence = new FibonacciSequence(1, 5000);
        assertEquals(numbers, fibonacciSequence.getNumbers());
    }

}