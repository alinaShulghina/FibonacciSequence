package com.softserve.edu.fibonacci;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alin- on 12.12.2017.
 */
public class FibonacciSequence {

    private List<Integer> numbers;

    /*
        The order of input is not important. a is min and b is max
     */
    public FibonacciSequence(int a, int b) {
        numbers = new ArrayList<>();
        generateSequence(Math.min(a, b), Math.max(a, b));
    }

    public List<Integer> getNumbers() {
        return numbers;
    }

    private void generateSequence(int min, int max) {
        int first = 0;
        int second = 1;
        int third = 1;
        while (third < max) {
            if (third > min) {
                numbers.add(third);
            }
            third = first + second;
            first = second;
            second = third;
        }
    }

}
