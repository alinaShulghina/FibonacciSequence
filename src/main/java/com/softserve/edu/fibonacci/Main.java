package com.softserve.edu.fibonacci;

/**
 * Created by alin- on 11.12.2017.
 */
public class Main {

    public static void main(String[] args) {
        try {
            int a = Integer.valueOf(args[0]);
            int b = Integer.valueOf(args[1]);
            FibonacciSequence sequence = new FibonacciSequence(a, b);
            sequence.getNumbers().forEach(System.out::println);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.print("You should input 2 params!");
        } catch (NumberFormatException e) {
            System.out.print("Strings are not allowed!");
        }
    }


}
